package com.zeroqualitygames.spaceinvaders;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import com.zeroqualitygames.spaceinvaders.Game;

public class GameMain extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private Game game;

	public GameMain(String name) {
		super(name);
		game = new Game();
		
		setSize(Game.WIDTH + JFrame.WIDTH , Game.HEIGHT + JFrame.HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIgnoreRepaint(true);
		
		setLocationRelativeTo(null);
		setVisible(true);
		
		setLayout(new BorderLayout());
		add(game, BorderLayout.CENTER);
		
		pack();
		game.start();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new GameMain("Space Invaders by Zero Quality Games");
	}

}
