package com.zeroqualitygames.spaceinvaders.resources;

import java.applet.Applet;
import java.applet.AudioClip;

public class Sound {
	
	private AudioClip clip;
	private String name;

	public Sound(String name) {
		this.name = name;
		try {
			clip = Applet.newAudioClip(this.getClass().getResource(name));
		}
		catch (Exception e) {
			System.out.println("Could not load the file: " + name);
		}
	}
	
	public void play() {
		try {
			new Thread(new Runnable() {
	
				@Override
				public void run() {
					clip.play();
				}
				
			}).start();
			}
		catch (Exception e) {
			System.out.println("The sound file: " + name + " could not be played");
		}
	}

}
