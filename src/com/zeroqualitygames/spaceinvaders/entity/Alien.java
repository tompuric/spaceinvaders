package com.zeroqualitygames.spaceinvaders.entity;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.ArrayList;

import com.zeroqualitygames.spaceinvaders.Game;


public class Alien extends Entity {
	long startTime;
	int level;

	public Alien(int x, int y, long startTime, int level) {
		width = 30;
		height = 30;
		
		this.x = x;
		this.y = y;
		this.startTime = startTime;
		this.level = level;
		vx = 0.5f + (level/10);
		
		rect = new Rectangle((int) x, (int) y, width, height);
	}
	
	
	public void tick() {
		move(vx, vy);
		
		ArrayList<Entity> entities = game.getEntities();
		for (Entity e: entities) {
			if (e instanceof Missile) {
				if (rect.intersects(e.rect)) {
					Game.SCORE++;
					e.remove();
					die();
					return;
				}
			}
		}
	}
	
	public void reverse() {
		vx = -vx;
		y += 5 + (5*level)/2;
		if (y + height > game.getEntities().get(0).y)
			Game.GAMEOVER = true;
	}
	
	public void die() {
		Game.destroy.play();
		super.remove();
	}
	
	public void render(Graphics2D g) {
		g.fillRect((int) x, (int) y, width, height);
	}
}
