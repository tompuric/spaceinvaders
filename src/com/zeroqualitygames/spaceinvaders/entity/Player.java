package com.zeroqualitygames.spaceinvaders.entity;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import com.zeroqualitygames.spaceinvaders.Game;
import com.zeroqualitygames.spaceinvaders.InputManager;
import com.zeroqualitygames.spaceinvaders.menu.GameOver;

public class Player extends Entity {
	
	private InputManager input;
	
	public static final int LEFT = -2;
	public static final int RIGHT = 2;

	public Player(InputManager input) {
		this.input = input;
		
		width = 30;
		height = 30;
		
		x = Game.WIDTH/2 - width/2;
		y = Game.HEIGHT - height;
		System.out.println(y);
		rect = new Rectangle( (int) x, (int) y, width, height);
	}
	
	public void tick() {
		if (input.left.pressed)
			vx = LEFT;
		else if (input.right.pressed)
			vx = RIGHT;
		else
			vx = 0;
		
		if (x + vx < 0 || x + vx > Game.WIDTH - width)
			return;
		
		super.move(vx,  vy);
	}
	
	public void remove() {
		super.remove();
		game.setMenu(new GameOver());
	}
	
	public void render(Graphics2D g) {
		g.setColor(Color.BLUE);
		g.fillRect((int) x, (int) y, width, height);
		g.setColor(Color.BLACK);
		g.drawRect((int) x, (int) y, width, height);
	}

}
