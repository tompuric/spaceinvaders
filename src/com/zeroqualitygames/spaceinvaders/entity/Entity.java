package com.zeroqualitygames.spaceinvaders.entity;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import com.zeroqualitygames.spaceinvaders.Game;

public class Entity {
	public float x;
	public float y;
	
	public int width;
	public int height;
	
	public float vx;
	public float vy;
	
	public Game game;	
	public Rectangle rect;
	
	public boolean removed = false;

	public Entity() {
		// TODO Auto-generated constructor stub
	}
	
	public void init(Game game) {
		this.game = game;
	}
	
	public void remove() {
		removed = true;
	}
	
	public void tick() {
		
	}
	
	public void move(float vx, float vy) {
		if (vx != 0 && vy != 0)
			return;
		x += vx;
		y += vy;
		rect.setLocation( (int) x, (int) y);
	}
	
	public void render(Graphics2D g) {
		
	}

}
