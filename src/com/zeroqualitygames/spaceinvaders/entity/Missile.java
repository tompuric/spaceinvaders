package com.zeroqualitygames.spaceinvaders.entity;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import com.zeroqualitygames.spaceinvaders.Game;

public class Missile extends Entity {
	
	private final int SPEED = -2;

	public Missile(int x) {
		width = 10;
		height = 30;
		
		this.x = x - width/2;
		y = Game.HEIGHT - height;
		
		rect = new Rectangle((int) x, (int) y, width, height);
	}
	
	public void tick() {
		super.move(0, SPEED);
		
		if (y + height < 0)
			remove();
	}
	
	public void remove() {
		super.remove();
	}
	
	public void render(Graphics2D g) {
		g.setColor(Color.RED);
		g.fillRect((int) x, (int) y, width, height);
	}

}
