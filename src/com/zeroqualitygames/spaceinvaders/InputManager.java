package com.zeroqualitygames.spaceinvaders;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

public class InputManager implements KeyListener {
	
	public class Key {
		public Boolean pressed = false;
		
		public Key() {
			keys.add(this);
		}
		
		public void toggle(boolean pressed) {
			this.pressed = pressed;
		}
	}

	public ArrayList<Key> keys = new ArrayList<Key>();
	
	public Key left = new Key();
	public Key right = new Key();
	public Key shoot = new Key();
	public Key anykey = new Key();
	
	public InputManager(Game game) {
		game.addKeyListener(this);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		toggle(e, true);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		toggle(e, false);
	}
	
	public void toggle(KeyEvent e, Boolean pressed) {
		anykey.toggle(pressed);
		
		if (e.getKeyCode() == KeyEvent.VK_LEFT) left.toggle(pressed);
		if (e.getKeyCode() == KeyEvent.VK_A) left.toggle(pressed);
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) right.toggle(pressed);
		if (e.getKeyCode() == KeyEvent.VK_D) right.toggle(pressed);
		
		if (e.getKeyCode() == KeyEvent.VK_SPACE) shoot.toggle(pressed);
		
	}
}
