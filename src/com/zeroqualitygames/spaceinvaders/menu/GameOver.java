package com.zeroqualitygames.spaceinvaders.menu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import com.zeroqualitygames.spaceinvaders.Game;


public class GameOver extends Menu {
	
	long startTime = System.currentTimeMillis();
	
	public GameOver() {

	}
	
	public void tick() {
		if (input.anykey.pressed && System.currentTimeMillis() - startTime > 2000) {
			game.setMenu(null);
			game.restart();
			Game.SCORE = 0;
			Game.level = 0;
			Game.GAMEOVER = false;
		}
	}
	
	public void render(Graphics2D g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, Game.WIDTH, Game.HEIGHT);
		g.setColor(Color.BLACK);
		g.setFont(new Font("Arial", Font.BOLD, 32));
		g.drawString("Score: " + Game.SCORE, 170, 350);
		g.drawString("Press anykey to start again", 30, 250);
	}

}
