package com.zeroqualitygames.spaceinvaders.menu;

import java.awt.Graphics2D;

import com.zeroqualitygames.spaceinvaders.Game;
import com.zeroqualitygames.spaceinvaders.InputManager;

public class Menu {
	
	protected Game game;
	protected InputManager input;

	public Menu() {
		// TODO Auto-generated constructor stub
	}
	
	public void init(Game game, InputManager input) {
		this.game = game;
		this.input = input;
	}
	
	public void tick() {
		
	}
	
	public void render(Graphics2D g) {
		
	}

}
