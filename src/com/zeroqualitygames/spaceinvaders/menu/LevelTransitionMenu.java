package com.zeroqualitygames.spaceinvaders.menu;

import java.awt.Font;
import java.awt.Graphics2D;

import com.zeroqualitygames.spaceinvaders.Game;

public class LevelTransitionMenu extends Menu {
	long startTime;

	public LevelTransitionMenu() {
		startTime = System.currentTimeMillis();
	}
	
	public void tick() {
		if (System.currentTimeMillis() - startTime > 3000)
			game.setMenu(null);
	}
	
	public void render(Graphics2D g) {
		g.setFont(new Font("Arial", Font.BOLD, 46));
		g.drawString("" + (3 - (System.currentTimeMillis() - startTime)/1000), Game.WIDTH/2 - 20, Game.HEIGHT/2 + 50);
	}

}
